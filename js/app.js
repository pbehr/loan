window.onload = function() {

    homeInfo = {
        changeValue: function() {
            var propertyValue = homeInfo.homeValueElem.value;
            homeInfo.loanAmountElem.value = propertyValue - homeInfo.downpaymentElem.value;
            loanInfo.calcPayment();
        },
        changeDownpayment: function() {
            homeInfo.loanAmountElem.value = homeInfo.homeValueElem.value - homeInfo.downpaymentElem.value;
            loanInfo.calcPayment();
        },
        changeLoanAmount: function() {
            var loanAmount = homeInfo.loanAmountElem.value;
            var elementToChange = document.getElementById('loan_adjust_button').value;
            switch (elementToChange) {
                case 'value' : 
                  homeInfo.homeValueElem.value = parseInt(loanAmount) + parseInt(homeInfo.downpaymentElem.value);
                  break;
                case 'down' :
                  homeInfo.downpaymentElem.value = parseInt(homeInfo.homeValueElem.value) - parseInt(loanAmount);
                  break;
            }
            
            loanInfo.calcPayment();
        },
        calcPercent: function(percent) {
            homeInfo.downpaymentElem.value = Math.round(homeInfo.homeValueElem.value * (percent/100));
            this.changeDownpayment();   
        },
        changeLoanAdjust: function(element) {
            var btn = document.getElementById('loan_adjust_button');
            switch (element) {
                case 'value' :
                   btn.value = 'value';
                   btn.innerHTML = 'Value <span class="caret"></span>';
                   break;
                case 'down' :
                   btn.value = 'down';
                   btn.innerHTML = 'Down <span class="caret"></span>';
                   break;
            }
        },
        homeValueElem:  document.getElementById('propertyValue'),
        downpaymentElem: document.getElementById('downpayment'),
        loanAmountElem: document.getElementById('loanAmount'),
    }

    loanInfo = {
        calcPayment: function() {
            var principal = parseInt(homeInfo.loanAmountElem.value);
            var interest = parseFloat(loanInfo.interestElem.value) / 100 / 12;
            var payments = parseInt(loanInfo.termElem.value) * 12;
            var x = Math.pow(1 + interest, payments);
            loanInfo.paymentElem.value = Math.round( (principal * x * interest)/(x-1) );
            scheduleInfo.showSchedule();
        },
        calcLoanAmount: function() {
            var payment = parseInt(this.value);
            var interest = parseFloat(loanInfo.interestElem.value) / 100 / 12;
            var payments = parseInt(loanInfo.termElem.value) * 12;
            var x = Math.pow(1 + interest, payments);
            homeInfo.loanAmountElem.value = Math.round( ((payment * x) - payment) / (x*interest) );
            homeInfo.changeLoanAmount();
            //homeInfo.downpaymentElem.value = parseInt(homeInfo.homeValueElem.value) - parseInt(homeInfo.loanAmountElem.value);
        },
        interestElem: document.getElementById('interestRate'),
        termElem: document.getElementById('interestTerm'),
        paymentElem: document.getElementById('scheduledPayment'),
        extraElem: document.getElementById('extraPayment'),
    }

    scheduleInfo = {
        calcSchedule: function() {
            var remaining = parseInt(homeInfo.loanAmountElem.value);
            var payment = parseFloat(loanInfo.paymentElem.value);
            var interest = parseFloat(loanInfo.interestElem.value) / 100 / 12;
            var payments = parseInt(loanInfo.termElem.value) * 12;
            var extra = parseInt(loanInfo.extraElem.value);
            if (isNaN(extra)) {
                extra = 0;
            }
            this.schedule = [];
            for (var i = 0; i < payments; i++) {
                var interestPayment = remaining * interest;
                var principalPayment = payment - interestPayment;
                 this.schedule[i] = {
                    balance : remaining,
                    interest : interestPayment,
                    payment : payment,
                    extra : extra,
                };
                remaining -= (principalPayment + extra);
            }        
            return this.schedule;
        },
        showSchedule: function() {
            me = scheduleInfo;
            me.calcSchedule();
            var tmpl = document.getElementById('payment'), 
                sched = document.getElementById('paymentSchedule'),
                row = tmpl.content.querySelectorAll("td");

            sched.innerHTML = "";

            for (var i=0; i<me.schedule.length; i++) {

                if (me.schedule[i].balance < 0) {
                    break;
                }

                row[0].textContent = i + 1;
                row[1].textContent = me.formatCurrency( me.schedule[i].balance );
                row[2].textContent = me.formatCurrency( me.schedule[i].payment );
                row[3].textContent = me.formatCurrency( me.schedule[i].interest );
                row[4].textContent = me.formatCurrency( me.schedule[i].extra );
                row[5].textContent = me.formatCurrency( (me.schedule[i].payment - me.schedule[i].interest) + me.schedule[i].extra );

                // clone the table row
                var clone = document.importNode(tmpl.content, true);

                // insert the new row into the schedule
                sched.appendChild(clone);
                
            }        

        },
        formatCurrency : function(number) {
            return number.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
        },
        paymentStart: document.getElementById('paymentStart'),
        schedule : []
    }

    init();
}

init = function() {
    addListeners();
    setDefaultValues();
    loanInfo.calcPayment();

    var date_input=$('input[name="date"]'); 
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
     var options={
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
    };
    date_input.datepicker(options);
}

addListeners = function() {
    homeInfo.homeValueElem.addEventListener('change', homeInfo.changeValue);
    homeInfo.downpaymentElem.addEventListener('change', homeInfo.changeDownpayment);
    homeInfo.loanAmountElem.addEventListener('change', homeInfo.changeLoanAmount);
    loanInfo.interestElem.addEventListener('change', loanInfo.calcPayment);
    loanInfo.termElem.addEventListener('change', loanInfo.calcPayment);
    loanInfo.paymentElem.addEventListener('change', loanInfo.calcLoanAmount);
    loanInfo.extraElem.addEventListener('change', scheduleInfo.showSchedule);
}

setDefaultValues = function() {
    loanInfo.interestElem.value = 5;
    loanInfo.termElem.value = 30;
    homeInfo.homeValueElem.value = 500000;
    homeInfo.calcPercent(20);
   // document.getElementById('paymentStart').valueAsDate = new Date();
}


